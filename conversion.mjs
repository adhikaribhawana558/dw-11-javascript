//Conversion from number  to String
// let age = 23
// let ageStr = String(age) //"23"
// console.log(ageStr)

// let age = 29
// let ageStr = String(age)
// console.log(ageStr)

//Conversion from string to number
// let age = "23"
// let ageNum = Number(age)  // 23
// console.log(ageNum)

// let age = "36"
// let ageNum = Number(age)
// console.log(ageNum)

//Conversion any to boolean
// console.log(Boolean("true"))
// console.log(Boolean("2"))
// console.log("")
// console.log(Boolean(`1`))

console.log(Boolean("false"))
console.log(Boolean("2222"))
console.log("/")
console.log(Boolean(''))

