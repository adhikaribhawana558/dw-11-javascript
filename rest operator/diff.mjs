//spread operator=> wrapper opener (it is used to make new variable(data))
// eg let a1 = [1, 2, 3]
// let a2 = [5, 10, ...a1, 11]   //[5, 10, 1, 2, 3, 11]



//rest operator => takes rest of value (it is used in receiver section)
// let [a, ...b] = //[1, [2, 3]]