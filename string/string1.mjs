//  let name = "My name is Bhawana Adhikari"

//  console.log(name.length)  // 16
//  console.log(name.toUpperCase()) //BHAWANA ADHIKARI
//  console.log(name.toLowerCase())  //bhawana adhikari

// console.log(name.trimStart()) //"  BhaWana"
// console.log(name.trimEnd())  //"BhaWana  "
// console.log(name.trim())   //"BhaWana"

//includes
// console.log(name.includes("is")) //true
// console.log(name.startsWith("an")) //false
// console.log(name.startsWith("My")) //true
// console.log(name.endsWith(" Adhikari")) //true

//replace
// console.log(name.replaceAll(" ", ","))// "My,name,is,bhawana,adhikari"
// console.log(name.replaceAll("name ", "naam "))  //"My naam is Bhawana Adhikari"
// console.log(name.replace(" ", ", ")) //"My,name is Bhawana adhikari"