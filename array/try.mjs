// make a arrow function that takes input as word "my name is nitan" and it must conver the input to camelCase like "My Name Is Nitan"
// let value = "my name is nitan"
// let outputAr = value.split(" ")
// let outputAr1 = Convert.toCamelCaseConvention(value)
// console.log(outputAr1)


// let firstLetterCapita = (input) => {
    //input =my => ["m","y"] =>["M","y"] ="My"
//     let inputAr = input.split(""); //["m","y"]
//     let outputAr = inputAr.map((value, i) => {
//       if (i === 0) {
//         return value.toUpperCase();
//       } else {
//         return value.toLowerCase();
//       }
//     }); //["M","y"]
//     let output = outputAr.join("");
//     return output;
//   };
//   console.log(firstLetterCapita("ram"));

// OR


let firstLetterCapital = (input) => {
    //input ="my" => ["m", "y"]=> ["M", "y"] => "My"
    let inputAr = input.split("") //["m", "y"]
    inputAr[0] = inputAr[0].toUpperCase() //["M", "y"]
    // console .log(inputAr)

    let output = inputAr.join("") //"My"
    return output
}
// console.log(firstLetterCapital("my"))

let isWordCapital =(input) => {
    //input = "my name is nitan"
    let inputAr = input.split(" ")  //"my", "name", "is", "nitan"
    //outputAr =["My", "Name", "Is", "Nitan"]
    let outputAr = inputAr.map((value, i) => {
        return firstLetterCapital(value) //"Is"
    })
    let output = outputAr.join(" ")
    return output
}
console.log(isWordCapital("my name is nitan"))
 