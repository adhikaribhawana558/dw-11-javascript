//              0     1        2
// let names = ["ram", "hari", "sita"]

//indexing
// console.log(names[0])
// console.log(names[1])
// console.log(names[2])

//              0        1        2
let names = ["ram", "100", "false"]

//indexing
console.log(names[0])
// console.log(names[1])
// console.log(names[2])

names[1]='200'
names[2] = true
console.log(names)

//define
// get each element
//change each element
// delete element

delete names[1];
console.log(names)
