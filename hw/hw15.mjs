// filter only truthy value
let ar1 = [true, 1, false, 8, true, 9]
let filteredAr1 = ar1.filter((value, i) => {
    if(typeof value === "boolean"){
        return true
    }
})
console.log(filteredAr1);