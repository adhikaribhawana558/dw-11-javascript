// filter string ["a",1,"b",3,"nitan",] = ["a","b","nitan"] (filter the string)
let ar1 = ["a",1,"b",3,"nitan"]

// let filteredAr1 = ["a", "b", "nitan"]
let filteredAr1 = ar1.filter((value, i) => {
    if (typeof value === "string"){
        return true
    } 
})
console.log(filteredAr1)