console.log(isNaN(1)) //1 is not  a number // false
console.log(isNaN(2)) //2 is not  a number // false
console.log(isNaN(3)) //3 is  a number
console.log(isNaN("ram")) //"ram" is not a number
console.log(isNaN(true)) //true is not a number but in binary 1
console.log(isNaN(false)) //false  is not a number but in binary )
// 1; //false 