// make a arrow function that takes input as age and it will throw error if age is less than 18 with message ("you are not allow to join team")
let age = (input) => {
    if(input<18){
        let error = new Error("you are not allow to join team")
        throw error
    }
    // else{
    //     return `You can join our team` // system crash
    // }
}
age(2)