let obj1 = {
    name: "bhawana",
    age: 26,                                        //key:value whole lai property
}

//converting object to array
//["name", "age"]
//["bhawana", 26]
//[["name" "bhawana"], ["age", 26]]

let keyArr = Object.keys(obj1)  //["name", "age"]
console.log(keyArr)

let valueArr = Object.values(obj1) //["bhawana", 26]
console.log(valueArr)

let prrValue = Object.entries(obj1)
console.log(prrValue)

