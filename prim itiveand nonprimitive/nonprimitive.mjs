//array, object, error, (are non primitive)=> (===)address are same vyo vne true natra false  => call by reference
// The type of non primitive is object
// let a = [1,2]   Here a and b have same address which store in same variable
// let b = [1,2]
// let c = a

// console.log(a===b) //true
// console.log(a===c) //false


// let ar1 = [1,2]
// let ar2 = [1,2]
// let ar3 = ar1
// ar1.push(5)
// ar2.push(6)
// console.log(ar1)
// console.log(ar2)

// let ar1 = [1,2]
// let ar2= ar1
// ar1 = [1,3]
// console.log(ar1)
// console.log(ar2)

//string,boolen,number,undefined,null
// let a=1
// let b=1
// let c=a
// console.log(1===1)//true
// console.log(undefined===undefined)//true
// console.log(null===null)//true
// console.log([1,2]===[1,2])//false
// let a=1
// let b=1
// console.log(a===b)//true
// let ar1=[3,4]
// let ar2=ar1
// let ar3=(ar1===ar2)//true
// console.log(ar1===ar2)//true
// console.log(ar1===ar3)//false
