let a = 5
let c = 9
{
    //A
    let a = 4
    let b = 10
    console.log(a)
}
console.log(a)




// let a = 3
// let c = 10
// {
    //A
//     let a = 4
//     let b = 10
//     console.log(a)
// }
// console.log(a)

//When you execute a program
//It has two phases
//1. Memory Allocation (It is done first)
//2. Code Execution (it is done after memory allocation)

//Global
//a=3
//c = 10

//A
//a= 4
//b= 10