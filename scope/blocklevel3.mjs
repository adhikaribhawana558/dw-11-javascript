{
    //a
    let a = 1
    {//b
        let a = 7
        console.log(a)
    }
    console.log(a)
}



// {
    //A
//     let a = 1
//     {//B
//         let a = 5
//         console.log(a)

//     }
//     console.log(a)
// }
//While calling variable first it search in its own block 
//if it doesnot found it search in its parent block and so on
//This is called scope chaining


//A*************
// a=1

//B*********8
// a = 5