// {
//     let a = 1
//     {
//         let a = 5
//     }
// }

{
    let b = 8
    {
        let b = 6
    }
}
//We cannot define same variable(name) in same block
//but we can define same variable(name) in different block
