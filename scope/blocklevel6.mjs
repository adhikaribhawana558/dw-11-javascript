// let a = 3
// {
    //A
//     console.log(a)
//     let a = 8
// }


let a = 3
{
    //A
    console.log(a)
     a = 8
}//a = 8

//cgl(a) will result error vbecause
//the variable will be searched in A block (but scope of a is from line number 7)

//Global
//a=3

//A
//a