let obj1 = {name: "bhawana", age: 26}
let obj2 = {address: "Dhumbarahi"}

// let obj3 = {isMarried: false, ...obj1, ...obj2}
let obj3 = {isMarried: false, ...obj1, obj2}

console.log(obj3) //{isMarried:false, name: 'bhawana', age:26}