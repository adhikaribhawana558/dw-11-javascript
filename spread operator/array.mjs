let basket1 = ["apple", "ball", "banana"]
let basket2 = ["orange", "mango"]

let basket3 = [...basket1, "a", "b"]     //...(open the wrapper)
// let basket4 = [...basket1, "a", ...basket2, "b"]
let basket4 = [...basket1, "a", basket2, "b"]
// "apple", "ball", "banana", "a", "orange", "mango", "b"

console.log(basket3)
console.log(basket4)
